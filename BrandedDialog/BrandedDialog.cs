﻿using System;

using Xamarin.Forms;

namespace BrandedDialog
{
	public class App : Application
	{
		public App ()
		{
			// The root page of your application
			MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Padding = new Thickness(80),
					Children = {
						new Button {
							Text = "Alert dialog",
							Command = new Command(async () => {
								await DependencyService.Get<IDisplayAlertService>()
									.ShowAlertConfirm("Title", "Content", "Confirm", "Cancel", (result) => 
										System.Diagnostics.Debug.WriteLine("Result: " + result.ToString()));
							})
						}
					}
				}
			};
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

