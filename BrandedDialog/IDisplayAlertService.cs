﻿using System;
using System.Threading.Tasks;

namespace BrandedDialog
{
	public interface IDisplayAlertService
	{
		Task ShowAlertConfirm (string title, string content, string confirmButton, string cancelButton,
			Action<bool> callback);
	}
}

