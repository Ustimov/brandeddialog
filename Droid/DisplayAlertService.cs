﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Widget;
using Android.Graphics;
using Xamarin.Forms;

[assembly: Dependency(typeof (BrandedDialog.Droid.DisplayAlertService))]

namespace BrandedDialog.Droid
{
	public class DisplayAlertService : IDisplayAlertService
	{
		public async Task ShowAlertConfirm(string title, string content, string confirmButton, string cancelButton,
			Action<bool> callback)
		{
			await Task.Run(() => AlertConfirm(title, content, confirmButton, cancelButton, callback));
		}

		private void AlertConfirm(string title, string content, string confirmButton, string cancelButton,
			Action<bool> callback)
		{
			var alert = new AlertDialog.Builder(Forms.Context);
			alert.SetTitle(title);
			alert.SetMessage(content);
			alert.SetPositiveButton(confirmButton, (sender, e) => { callback(true); });
			alert.SetNegativeButton(cancelButton, (sender, e) => { callback(false); });

			Device.BeginInvokeOnMainThread(() =>
				{
					var dialog = alert.Show();
					BrandAlertDialog(dialog);
				});
		}

		private static void BrandAlertDialog(Dialog dialog)
		{
			try
			{
				var resources = dialog.Context.Resources;

				// Here you can setup dialog colors
				var color = Android.Graphics.Color.Red;
				var background = Android.Graphics.Color.Red;

				var alertTitleId = resources.GetIdentifier("alertTitle", "id", "android");
				var alertTitle = (TextView) dialog.Window.DecorView.FindViewById(alertTitleId);
				alertTitle.SetTextColor(color); // change title text color

				var titleDividerId = resources.GetIdentifier("titleDivider", "id", "android");
				var titleDivider = dialog.Window.DecorView.FindViewById(titleDividerId);
				titleDivider.SetBackgroundColor(background); // change divider color
			}
			catch
			{
				// Can't change dialog brand color
			}
		}
	}
}

